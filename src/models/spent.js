const mongoose = require('mongoose')

const Spent = mongoose.model('Spents', {
    name: {
        type: String,
        required: true,
        trim: true
    },
    date: {
        type: String,
        required: true,
    },
    totalSpent: {
        type: Number,
        required: true
    },
    owner:{
        type:mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'User'
    }
})

module.exports = Spent