const express = require('express')
const Spent = require('../models/spent')
const auth = require('../middleware/auth')
const router = new express.Router()

router.post('/spents', auth, async (req, res) => {
    const spent = new Spent({
        ...req.body,
        owner: req.user._id
    })

    try {
        await spent.save()
        res.status(201).send(spent)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/spents', auth, async (req, res) => {
    const spents = await Spent.find({owner:req.user._id})
    try {
        if (!spents) {
            res.status(404).send()
        }
        res.status(200).send(spents)
    } catch (e) {
        res.status(200).send(e)
    }
})

module.exports = router